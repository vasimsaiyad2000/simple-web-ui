import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppHeaderComponent } from './headernavbar/headerbar.component';
import { AppLeftNavBarComponent } from './leftnavbar/leftnavbar.component';
import { SalesRepositoryListComponent } from './sales-repository/list/list.component';
import { SalesRepositoryCreateComponent } from './sales-repository/create/create.component';
import { CustomerRepositoryListComponent } from './customer-repository/list/list.component';
import { SalesPersonProfileListComponent } from './sales-persons-profile/list/list.component';
import { SalesPersonTargetsListComponent } from './sales-person-targets/list/list.component';
import { SalesPersonMatchListComponent } from './sales-person-match/list/list.component';

import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import * as more from 'highcharts/highcharts-more.src';
import * as solidGauge from 'highcharts/modules/solid-gauge.src';


@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        AppHeaderComponent,
        AppLeftNavBarComponent,
        SalesRepositoryListComponent,
        SalesRepositoryCreateComponent,
        CustomerRepositoryListComponent,
        SalesPersonProfileListComponent,
        SalesPersonTargetsListComponent,
        SalesPersonMatchListComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        ChartModule
    ],
    providers: [{ provide: HIGHCHARTS_MODULES, useFactory: () => [more, solidGauge] }],
    bootstrap: [AppComponent]
})
export class AppModule { }
