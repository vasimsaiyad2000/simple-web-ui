import { Component, OnInit } from '@angular/core';
import { SalesPersonsProfile } from '../../models/sales.persons.profile';
import { PersonMatch } from '../../models/person.match';
import { SalesPersonsProfileService } from '../../service/sales.persons.profile.service';

@Component({
    selector: 'app-sales-person-match-list',
    templateUrl: './list.component.html'
})
export class SalesPersonMatchListComponent implements OnInit {

    model: any = {};
    genders: any = [];
    languages: any = [];
    customerTypes: any = [];
    priceLevels: any = [];
    propertyTypes: any = [];
    transactionTypes: any = [];

    salesPersonProfiles: SalesPersonsProfile[];
    personMathch: PersonMatch = new PersonMatch();

    constructor(private salesPersonsProfileService: SalesPersonsProfileService) { }

    ngOnInit() {

        // Getting genders to bind to UI
        this.genders = this.personMathch.getGenders();
        this.model.gender = this.genders[0].value;

        // Getting languages to bind to UI
        this.languages = this.personMathch.getLanguages();
        this.model.language = this.languages[0].value;

        // Getting customer types to bind to UI
        this.customerTypes = this.personMathch.getCustomerTypes();
        this.model.customerType = this.customerTypes[0].value;

        // Getting transaction types to bind to UI
        this.transactionTypes = this.personMathch.getTransactionTypes();
        this.model.transactionType = this.transactionTypes[0].value;

        // Getting price level to bind to UI.
        this.priceLevels = this.personMathch.getPriceLevels();
        this.model.priceLevel = this.priceLevels[0].value;

        // Getting property types to bind to UI.
        this.propertyTypes = this.personMathch.getPropertyTypes();
        this.model.propertyType = this.propertyTypes[0].value;

        this.model.age = '';
    }

    match() {
        this.salesPersonsProfileService.search(this.model).subscribe(
            salesPersonProfiles => {
                this.salesPersonProfiles = salesPersonProfiles;
            },
            err => {
                console.log(err);
            }
        );
    }


}
