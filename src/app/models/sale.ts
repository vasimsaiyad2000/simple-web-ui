import { Agent } from './agent';
import { Property } from './property';

export class Sale {

    id: number;
    units: number;
    unitPrice: number;
    totalPrice: number;
    dealDate: string;
    codeAgent: Agent;
    codeProperty: Property;

    constructor(id: number, units: number,
        unitPrice: number, totalPrice: number,
        dealDate: string, codeAgent: Agent,
        codeProperty: Property) {

        this.id = id;
        this.units = units;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
        this.dealDate = dealDate;
        this.codeAgent = codeAgent;
        this.codeProperty = codeProperty;
    }
}
