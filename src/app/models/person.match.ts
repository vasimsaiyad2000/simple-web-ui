export class PersonMatch {

    genders: any = [];
    languages: any = [];
    customerTypes: any = [];
    transactionTypes: any = [];
    priceLevels: any = [];
    propertyTypes: any = [];

    constructor() { }

    getGenders() {

        this.genders = [{
            'name': 'Select Gender',
            'value': ''
        }, {
            'name': 'Male',
            'value': 'Male'
        }, {
            'name': 'Female',
            'value': 'Female'
        }, {
            'name': 'LR',
            'value': 'LR'
        }];

        return this.genders;
    }

    getLanguages() {

        this.languages = [{
            'name': 'Select Language',
            'value': ''
        }, {
            'name': 'Hebrew',
            'value': 'Hebrew'
        }, {
            'name': 'English',
            'value': 'English'
        }, {
            'name': 'Russian',
            'value': 'Russian'
        }];

        return this.languages;
    }

    getCustomerTypes() {

        this.customerTypes = [{
            'name': 'Select Customer Type',
            'value': ''
        }, {
            'name': 'Private',
            'value': 'Private'
        }, {
            'name': 'Institutional',
            'value': 'Institutional'
        }, {
            'name': 'Group Purchase',
            'value': 'Group Purchase'
        }];

        return this.customerTypes;
    }

    getTransactionTypes() {

        this.transactionTypes = [{
            'name': 'Select Transaction Type',
            'value': ''
        }, {
            'name': 'Cash',
            'value': 'Cash'
        }, {
            'name': 'Mortgage',
            'value': 'Mortgage'
        }, {
            'name': 'No Mortgage',
            'value': 'No Mortgage'
        }];

        return this.transactionTypes;
    }

    getPriceLevels() {

        this.priceLevels = [{
            'name': 'Select Price Level',
            'value': ''
        }, {
            'name': 'Lower (0.5-5)',
            'value': 'Lower'
        }, {
            'name': 'Medium (3-7)',
            'value': 'Medium'
        }, {
            'name': 'High (7-20)',
            'value': 'High'
        }];

        return this.priceLevels;
    }

    getPropertyTypes() {

        this.propertyTypes = [{
            'name': 'Select Property Type',
            'value': ''
        }, {
            'name': 'Apartment Towers',
            'value': 'Apartment Towers'
        }, {
            'name': 'Land Investment',
            'value': 'Land Investment'
        }, {
            'name': 'Thrift',
            'value': 'Thrift'
        }, {
            'name': 'Private Homes',
            'value': 'Private Homes'
        }];

        return this.propertyTypes;
    }
}
