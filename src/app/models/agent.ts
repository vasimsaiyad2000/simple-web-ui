export class Agent {
    codeAgent: number;
    agentName: string;
    agentEmail: string;

    constructor(codeAgent: number, agentName: string, agentEmail: string) {
        this.codeAgent = codeAgent;
        this.agentName = agentName;
        this.agentEmail = agentEmail;
    }
}
