export class Gender {

    codeGender: number;
    descGender: string;

    constructor(codeGender: number, descGender: string) {
        this.codeGender = codeGender;
        this.descGender = descGender;
    }
}
