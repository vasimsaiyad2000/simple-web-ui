export class Language {
    codeLang: number;
    descLang: string;

    constructor(codeLang: number, descLang: string) {
        this.codeLang = codeLang;
        this.descLang = descLang;
    }
}
