export class CustomerType {

    codeCustomerType: number;
    descCustomerType: string;

    constructor(codeCustomerType: number, descCustomerType: string) {
        this.codeCustomerType = codeCustomerType;
        this.descCustomerType = descCustomerType;
    }
}
