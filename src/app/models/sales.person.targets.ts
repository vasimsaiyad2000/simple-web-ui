export class SalesPersonTargets {
    quantityTarget: number;
    statusTargetNumber: number;
    statusTargetPercentage: number;
    weightInPercentage: number;
    completionDate: number;
    counterUntilDate: number;
    totalScore: number;
    projectName: string;
    targetName: string;

    constructor(quantityTarget: number, statusTargetNumber: number,
        statusTargetPercentage: number, weightInPercentage: number,
        completionDate: number, counterUntilDate: number,
        totalScore: number, projectName: string,
        targetName: string) {

        this.quantityTarget = quantityTarget;
        this.statusTargetNumber = statusTargetNumber;
        this.statusTargetPercentage = statusTargetPercentage;
        this.weightInPercentage = weightInPercentage;
        this.completionDate = completionDate;
        this.counterUntilDate = counterUntilDate;
        this.totalScore = totalScore;
        this.projectName = projectName;
        this.targetName = targetName;
    }
}
