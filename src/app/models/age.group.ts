export class AgeGroup {
    codeAgeGroup: number;
    descAgeGroup: string;

    constructor(codeAgeGroup: number, descAgeGroup: string) {
        this.codeAgeGroup = codeAgeGroup;
        this.descAgeGroup = descAgeGroup;
    }
}
