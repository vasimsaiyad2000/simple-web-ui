import { Language } from './language';
import { Gender } from './gender';
import { PropertyType } from './property.type';
import { CustomerType } from './customer.type';
import { AgeGroup } from './age.group';
import { PriceRange } from './price.range';

export class SalesPersonsProfile {
    codeLang: Language;
    codeGender: Gender;
    codePropertyType: PropertyType;
    codeCustomerType: CustomerType;
    codeAgeGroup: AgeGroup;
    codePriceRange: PriceRange;
    codeAgent: number;
    agentName: string;
    targetName: string;

    constructor(codeLang: Language, codeGender: Gender,
        codePropertyType: PropertyType, codeCustomerType: CustomerType,
        codeAgeGroup: AgeGroup, codePriceRange: PriceRange,
        codeAgent: number, agentName: string,
        targetName: string) {

        this.codeLang = codeLang;
        this.codeGender = codeGender;
        this.codePropertyType = codePropertyType;
        this.codeCustomerType = codeCustomerType;
        this.codeAgeGroup = codeAgeGroup;
        this.codePriceRange = codePriceRange;
        this.codeAgent = codeAgent;
        this.agentName = agentName;
        this.targetName = this.targetName;
    }
}
