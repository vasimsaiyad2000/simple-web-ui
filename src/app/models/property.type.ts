export class PropertyType {

  codePropertyType: number;
  descPropertyType: string;

  constructor(codePropertyType: number, descPropertyType: string) {
    this.codePropertyType = codePropertyType;
    this.descPropertyType = descPropertyType;
  }
}
