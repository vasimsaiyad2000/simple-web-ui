export class PriceRange {

    codePriceRange: number;
    descPriceRange: string;

    constructor(codePriceRange: number, descPriceRange: string) {
        this.codePriceRange = codePriceRange;
        this.descPriceRange = descPriceRange;
    }
}
