import { Language } from './language';
import { Gender } from './gender';
import { PropertyType } from './property.type';
import { CustomerType } from './customer.type';

export class Customer {
    codeCustomer: number;
    customerName: string;
    customerPhone: string;
    customerEmail: string;
    codeLang: Language;
    codeGender: Gender;
    codePropertyType: PropertyType;
    codeCustomerType: CustomerType;

    constructor(codeCustomer: number, customerName: string,
        customerPhone: string, customerEmail: string,
        codeLang: Language, codeGender: Gender,
        codePropertyType: PropertyType, codeCustomerType: CustomerType) {

        this.codeCustomer = codeCustomer;
        this.customerName = customerName;
        this.customerPhone = customerPhone;
        this.customerEmail = customerEmail;
        this.codeLang = codeLang;
        this.codeGender = codeGender;
        this.codePropertyType = codePropertyType;
        this.codeCustomerType = codeCustomerType;
    }
}
