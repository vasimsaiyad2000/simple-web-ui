export class Property {

    codeProperty: number;
    descProperty: string;

    constructor(codeProperty: number, descProperty: string) {
        this.codeProperty = codeProperty;
        this.descProperty = descProperty;
    }
}
