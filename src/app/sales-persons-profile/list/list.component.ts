import { Component, OnInit } from '@angular/core';
import { SalesPersonsProfile } from '../../models/sales.persons.profile';
import { SalesPersonsProfileService } from '../../service/sales.persons.profile.service';

@Component({
    selector: 'app-sales-persons-details-list',
    templateUrl: './list.component.html',
    providers: [SalesPersonsProfileService]
})
export class SalesPersonProfileListComponent implements OnInit {

    salesPersonProfiles: SalesPersonsProfile[] = [];

    constructor(private salesPersonsProfileService: SalesPersonsProfileService) {

    }

    ngOnInit() {
        this.getAllSalesPersonProfiles();
    }

    getAllSalesPersonProfiles() {
        this.salesPersonsProfileService.finaAll().subscribe(
            salesPersonProfiles => {
                this.salesPersonProfiles = salesPersonProfiles;
            },
            err => {
                console.log(err);
            }
        );
    }
}
