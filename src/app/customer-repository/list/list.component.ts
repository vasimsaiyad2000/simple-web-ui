import { Component, OnInit } from '@angular/core';
import { Customer } from '../../models/customer';
import { CustomerService } from '../../service/customer.service';
import { ExcelService } from '../../utility/excel.service';

@Component({
    selector: 'app-customers-list',
    templateUrl: './list.component.html',
    providers: [CustomerService]
})
export class CustomerRepositoryListComponent implements OnInit {
    customers: Customer[] = [];

    constructor(private customerService: CustomerService,
        private excelService: ExcelService) { }

    ngOnInit() {
        this.getAllCustomers();
    }

    getAllCustomers() {
        this.customerService.findAll().subscribe(
            customers => {
                this.customers = customers;
            },
            err => {
                console.log(err);
            }
        );
    }

    exportAsExcel() {
        this.excelService.export(this.customers, 'customers');
    }
}
