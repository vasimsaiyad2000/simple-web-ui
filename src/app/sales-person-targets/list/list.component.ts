import { Component, OnInit } from '@angular/core';
import { SalesPersonTargets } from '../../models/sales.person.targets';
import { SalesPersonTargetsService } from '../../service/sales.person.targets.service';
import { ExcelService } from '../../utility/excel.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-sales-person-targets-list',
    templateUrl: './list.component.html',
    providers: [SalesPersonTargetsService]
})
export class SalesPersonTargetsListComponent implements OnInit {

    agentId: number;
    salesPersonTargets: SalesPersonTargets[] = [];

    constructor(private route: ActivatedRoute,
        private excelService: ExcelService,
        private salesPersonTargetService: SalesPersonTargetsService) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.agentId = +params['id'];
        });

        this.getTargetsByAgentId(this.agentId);
    }

    getTargetsByAgentId(agentId) {
        this.salesPersonTargetService.findByAgentId(agentId).subscribe(
            salesPersonTargets => {
                this.salesPersonTargets = salesPersonTargets;
            },
            err => {
                console.log(err);
            }
        );
    }

    exportAsExcel() {
        this.excelService.export(this.salesPersonTargets, 'sales-person-targets');
    }
}
