import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { ChartService } from '../utility/chart.service';
import { DashboardService } from '../service/dashboard.service';
import { SalesPersonsProfileService } from '../service/sales.persons.profile.service';
import { SalesPersonsProfile } from '../models/sales.persons.profile';

@Component({
    // tslint:disable-next-line:indent
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

    salesPersonProfiles: SalesPersonsProfile[] = [];
    model: any = {};

    sales: Chart;
    targets: Chart;
    counters: Chart;

    constructor(private dashboardService: DashboardService,
        private salesPersonsProfileService: SalesPersonsProfileService,
        private chartService: ChartService) { }

    ngOnInit() {
        this.getSalesPersons();
    }

    getDashboard(id) {
        this.dashboardService.finaBySalesPersonId(id).subscribe(
            model => {
                this.model = model;
                this.sales = this.chartService.getGaugeChart('Sales', this.model.salesAmount);
                this.targets = this.chartService.getGaugeChart('Targets', this.model.targetScores);
                this.counters = this.chartService.getGaugeChart('Counter from beginning', this.model.counterFromDate);
            },
            err => {
                console.log(err);
            }
        );
    }

    getSalesPersons() {
        this.salesPersonsProfileService.finaAll().subscribe(
            salesPersonProfiles => {
                this.salesPersonProfiles = salesPersonProfiles;
            },
            err => {
                console.log(err);
            }
        );
    }
}
