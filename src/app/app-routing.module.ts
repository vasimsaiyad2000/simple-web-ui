import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SalesRepositoryListComponent } from './sales-repository/list/list.component';
import { SalesRepositoryCreateComponent } from './sales-repository/create/create.component';
import { CustomerRepositoryListComponent } from './customer-repository/list/list.component';
import { SalesPersonProfileListComponent } from './sales-persons-profile/list/list.component';
import { SalesPersonTargetsListComponent } from './sales-person-targets/list/list.component';
import { SalesPersonMatchListComponent } from './sales-person-match/list/list.component';

const routes: Routes = [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'sales-repository/list', component: SalesRepositoryListComponent },
    { path: 'sales-repository/create', component: SalesRepositoryCreateComponent },
    { path: 'customer-repository/list', component: CustomerRepositoryListComponent },
    { path: 'sales-persons-profile/list', component: SalesPersonProfileListComponent },
    { path: 'sales-person-targets/list/:id', component: SalesPersonTargetsListComponent },
    { path: 'sales-person-match/list', component: SalesPersonMatchListComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '/dashboard' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
