import { SalesPersonTargets } from '../models/sales.person.targets';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SalesPersonTargetsService {

    private apiUrl = environment.url + '/api/target';

    constructor(private http: HttpClient) {
        // Do nothing
    }

    findByAgentId(id): Observable<SalesPersonTargets[]> {
        return this.http.get<SalesPersonTargets[]>(this.apiUrl + '/' + id);
    }
}
