import { Injectable } from '@angular/core';
import { Customer } from '../models/customer';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CustomerService {

    private customersUrl = environment.url + '/api/customers';
    private customerUrl = environment.url + '/api/customer';

    constructor(private http: HttpClient) {
        // Do nothing
    }

    findAll(): Observable<Customer[]> {
        return this.http.get<Customer[]>(this.customersUrl);
    }

    findById(id): Observable<Customer> {
        return this.http.get<Customer>(this.customerUrl + '/' + id);
    }
}
