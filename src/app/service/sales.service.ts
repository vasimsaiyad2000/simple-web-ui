import { Injectable } from '@angular/core';
import { Sale } from '../models/sale';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SalesService {

    private apiUrl = environment.url + '/api/activities';
    private newSaleUrl = environment.url + '/api/addActivity';

    constructor(private http: HttpClient) {
        // Do nothing
    }

    finaAll(): Observable<Sale[]> {
        return this.http.get<Sale[]>(this.apiUrl);
    }

    save(model: any): Observable<any> {
        return this.http.post(this.newSaleUrl, model);
    }
}
