import { Injectable } from '@angular/core';
import { SalesPersonsProfile } from '../models/sales.persons.profile';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SalesPersonsProfileService {

    private apiUrl = environment.url + '/api/sales';
    private matchUrl = environment.url + '/api/match';

    constructor(private http: HttpClient) {
        // Do nothing
    }

    finaAll(): Observable<SalesPersonsProfile[]> {
        return this.http.get<SalesPersonsProfile[]>(this.apiUrl);
    }

    search(model: any): Observable<SalesPersonsProfile[]> {

        let params = new HttpParams();

        if (model.gender) {
            params = params.append('gender', model.gender);
        }

        if (model.language) {
            params = params.append('language', model.language);
        }

        if (model.customerType) {
            params = params.append('ctype', model.customerType);
        }

        if (model.transactionType) {
            params = params.append('ttype', model.transactionType);
        }

        if (model.priceLevel) {
            params = params.append('plevel', model.priceLevel);
        }

        if (model.propertyType) {
            params = params.append('ptype', model.propertyType);
        }

        if (model.age) {
            params = params.append('age', model.age);
        }

        return this.http.get<SalesPersonsProfile[]>(this.matchUrl, { params });
    }
}
