import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Property } from '../models/property';

@Injectable({
    providedIn: 'root'
})
export class PropertyService {

    private apiUrl = environment.url + '/api/properties';

    constructor(private http: HttpClient) {
        // Do nothing
    }

    finaAll(): Observable<Property[]> {
        return this.http.get<Property[]>(this.apiUrl);
    }
}
