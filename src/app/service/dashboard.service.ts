import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {


    private apiUrl = environment.url + '/api/dashboard';

    constructor(private http: HttpClient) {
        // Do nothing
    }

    finaBySalesPersonId(id) {
        return this.http.get(this.apiUrl + '/' + id);
    }
}
