import { Component, OnInit } from '@angular/core';
import { SalesService } from '../../service/sales.service';
import { Router } from '@angular/router';
import { SalesPersonsProfile } from '../../models/sales.persons.profile';
import { Property } from '../../models/property';
import { Customer } from '../../models/customer';
import { PropertyService } from '../../service/property.service';
import { SalesPersonsProfileService } from '../../service/sales.persons.profile.service';
import { CustomerService } from '../../service/customer.service';

@Component({
    selector: 'app-sales-repository-create',
    templateUrl: './create.component.html'
})
export class SalesRepositoryCreateComponent implements OnInit {

    agents: SalesPersonsProfile[] = [];
    properties: Property[] = [];

    model: any = {};
    status: any = {};

    constructor(private salesService: SalesService,
        private salesPersonsProfileService: SalesPersonsProfileService,
        private propertyService: PropertyService,
        private customerService: CustomerService,
        private router: Router) {

    }

    ngOnInit() {
        this.getAgents();
        this.getProperties();
    }

    createSale() {
        this.model['total'] = (this.model.units * this.model.unitPrice) - this.model.discount;

        this.salesService.save(this.model).subscribe(
            res => {

                this.status.type = 'success';
                this.status.prefix = 'Success!';
                this.status.message = 'Sale created successfully';

                setTimeout(() => {
                    this.router.navigate(['/sales-repository/list']);
                }, 3000);
            },
            err => {
                this.status.type = 'error';
                this.status.prefix = 'Error!';
                this.status.message = 'Something went wrong!! Please try again';
            }
        );
    }

    getAgents() {
        this.salesPersonsProfileService.finaAll().subscribe(
            agents => {
                this.agents = agents;

                if (this.agents.length > 0) {
                    this.model['codeAgent'] = this.agents[0].codeAgent;
                }
            },
            err => {
                console.log(err);
            }
        );
    }

    getProperties() {
        this.propertyService.finaAll().subscribe(
            properties => {
                this.properties = properties;

                if (this.properties.length > 0) {
                    this.model['codeProperty'] = this.properties[0].codeProperty;
                }
            },
            err => {
                console.log(err);
            }
        );
    }

    getCustomerById(id: number) {
        this.customerService.findById(id).subscribe(
            customer => {
                this.model['customerName'] = customer.customerName;
            },
            err => {
                this.model['customerName'] = '';
                console.log(err);
            }
        );
    }
}


