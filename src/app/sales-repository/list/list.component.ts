import { Component, OnInit } from '@angular/core';
import { Sale } from '../../models/sale';
import { SalesService } from '../../service/sales.service';
import { ExcelService } from '../../utility/excel.service';


@Component({
    selector: 'app-sales-repository-list',
    templateUrl: './list.component.html',
    providers: [SalesService]
})
export class SalesRepositoryListComponent implements OnInit {

    sales: Sale[] = [];

    constructor(private salesService: SalesService,
        private excelService: ExcelService) {

    }

    ngOnInit() {
        this.getAllSales();
    }

    getAllSales() {
        this.salesService.finaAll().subscribe(
            sales => {
                this.sales = sales;
            },
            err => {
                console.log(err);
            }
        );
    }

    exportAsExcel() {
        this.excelService.export(this.sales, 'sales');
    }
}
